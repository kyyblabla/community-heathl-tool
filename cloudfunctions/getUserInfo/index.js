// 云函数入口文件
const cloud = require('wx-server-sdk')
cloud.init({
  env: cloud.DYNAMIC_CURRENT_ENV
})
const db = cloud.database({
  throwOnNotFound: false
})
const _ = db.command


// 云函数入口函数
exports.main = async (event, context) => {
  const wxContext = cloud.getWXContext()
  const {
    data: user
  } = await db.collection('user').doc(wxContext.OPENID).get()
  const communityId = user.baseInfo.community
  if (communityId) {
    const {
      data: community
    } = await db.collection('community').doc(communityId).get()
    Object.assign(user, {
      community
    })
  }
  const {
    total: homemateCount
  } = await db.collection('homemate').where({
    '_openid': wxContext.OPENID,
    deleted: _.neq(1)
  }).count()
  Object.assign(user, {
    homemateCount
  })
  return user
}