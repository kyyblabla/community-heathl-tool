# 小区健康助手

用于疫情期间小区内数据统计

## 功能截图

### 首页

![导入项目](./images/WX20200221-112817@2x.png)

### 数据统计

![导入项目](./images/WX20200221-112924@2x.png)

### 我的

![导入项目](./images/WX20200221-112946@2x.png)


# 导入项目
填入个人申请的的小程序appid
![导入项目](./images/import.png)

# 开通云开发
这是云开发的快速启动指引，其中演示了如何上手使用云开发的三大基础能力：

- 数据库：一个既可在小程序前端操作，也能在云函数中读写的 JSON 文档型数据库
- 文件存储：在小程序前端直接上传/下载云端文件，在云开发控制台可视化管理
- 云函数：在云端运行的代码，微信私有协议天然鉴权，开发者只需编写业务逻辑代码

## 参考文档

- [云开发文档](https://developers.weixin.qq.com/miniprogram/dev/wxcloud/basis/getting-started.html)


# 建立数据库

- user: 用户
- homemate: 家庭成员
- record: 每日记录
- community: 小区
- feedback: 反馈

## 配置数据权限

`user`表使用自定义权限：
```json
{
  "read": true,
  "write": "doc._id == auth.openid"
}
```


# 配置默认环境

```javascript
// app.js
App({
  async onLaunch(options) {
    console.log(options);
    if (!wx.cloud) {
      console.error('请使用 2.2.3 或以上的基础库以使用云能力')
    } else {
      wx.cloud.init({
        env: '填入默认的环境',
        traceUser: true,
      })
      this.globalData = {
        openId: null,
        env: options.env
      }
    }
  }
})
```

