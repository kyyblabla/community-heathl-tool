// miniprogram/pages/record/record.js
import {
  wxp
}
from '../../util/wxp'
import {
  format
}
from '../../util/index'
import Toast from '@vant/weapp/toast/toast';
const db = wx.cloud.database()

Page({
  mixins: [require('../../mixins/pageMixins')],
  /**
   * 页面的初始数据
   */
  data: {
    from: null,
    currentTime: format(new Date(), 'hh:mm'),
    currentDate: null,
    popup: null,
    homemates: [],
    homemate: null,
    columns: ['杭州', '宁波', '温州', '嘉兴', '湖州'],
    form: {
      tripTime: null,
      tripLocation: null,
      tripDesc: null,
      tempTime: null,
      tempValue: null,
    }
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: async function (options) {
    if (options._id) {
      const v = this.getPrevPage().getRecordById(options._id)
      const form = {
        tripTime: v.tripTime,
        tripLocation: v.tripLocation,
        tripDesc: v.tripDesc,
        tempTime: v.tempTime,
        tempValue: v.tempValue,
        _id: v._id
      }
      this.setData({
        homemate: v._homemate,
        form,
        from: options.from || null
      })
    }
    let titlePrefix = '';
    if (options.from == 'summary') {
      titlePrefix = "查看"
    } else {
      titlePrefix = options._id ? '修改' : '新增'
    }
    wx.setNavigationBarTitle({
      title: `${titlePrefix}记录 ${options.date}`
    })
    this.setData({
      currentDate: options.date
    })
  },
  onChangeHomemate(e) {
    const homemate = e.detail.value
    this.setData({
      'form.homemate': homemate.id,
      homemate
    })
    this.closePopup()
  },
  onChangeTripTime(e) {
    this.setData({
      'form.tripTime': e.detail
    })
    this.closePopup()
  },
  onChangeTempTime(e) {
    this.setData({
      'form.tempTime': e.detail
    })
    this.closePopup()
  },
  async saveInfo() {
    if (!this.data.homemate) {
      Toast('请选择成员')
      return
    }
    if (!this.data.form.tripTime && !this.data.form.tempTime) {
      Toast('请填写任一一项【本地出行记录】或【体温测量记录】')
      return
    }

    if (this.data.form.tripTime) {
      if (!this.data.form.tripLocation) {
        Toast('请填写出行地点')
        return
      }
    }

    if (this.data.form.tripLocation) {
      if (!this.data.form.tripTime) {
        Toast('请填写出行时间')
        return
      }
    }

    if (this.data.form.tempTime) {
      if (!this.data.form.tempValue) {
        Toast('请填写温度')
        return
      }
    }

    if (this.data.form.tempValue) {
      if (!this.data.form.tempTime) {
        Toast('请填写温度测量时间')
        return
      }
    }
    const data = {
      homemate: this.data.homemate._id,
      date: this.data.currentDate
    }
    Object.assign(data, this.data.form)
    if (data.tempValue) {
      data.tempValue = parseInt(data.tempValue)
    }
    const _id = this.data.form._id
    // 更新
    if (_id) {
      delete data._id
      delete data.date
      data.updateTime = db.serverDate()
      await db.collection('record').doc(_id).update({
        data
      })
    } else {
      data.addTime = db.serverDate()
      await db.collection('record').add({
        data
      })
    }
    this.getPrevPage().updateData()
    wx.navigateBack()
  },
  onSelectHomemates() {
    wx.navigateTo({
      url: '/pages/homemates/homemates?from=record',
    })
  },
  async deleteInfo() {
    await db.collection('record').doc(this.data.form._id).update({
      data: {
        deleted: 1,
        updateTime: db.serverDate()
      }
    })
    this.getPrevPage().updateData()
    wx.navigateBack()
  }
})