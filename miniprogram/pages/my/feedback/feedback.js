// miniprogram/pages/my/feedback/feedback.js
import {
  wxp
} from '../../../util/wxp'
const db = wx.cloud.database()
import Toast from '@vant/weapp/toast/toast';

Page({

  /**
   * 页面的初始数据
   */
  data: {
    text: null,
    tempFilePath: null
  },
  async upload() {
    const {
      tempFilePaths
    } = await wxp.chooseImage({
      count: 1,
      sizeType: ['original', 'compressed'],
      sourceType: ['album', 'camera'],
    })
    //http://tmp/wx06a479a53faedce6.o6zAJs54AiisH02FGzcA….RnSpCuUoc8xb2c936a25c8862719d5b2078cca18a55f.pn
    const fs = tempFilePaths[0].split('/')
    const fileName = fs[fs.length - 1]
    console.log(tempFilePaths);
    const {
      fileID
    } = await wxp.cloud.uploadFile({
      cloudPath: fileName, // 上传至云端的路径
      filePath: tempFilePaths[0], // 小程序临时文件路径
    })

    const {
      tempFilePath
    } = await wxp.cloud.downloadFile({
      fileID
    })

    this.setData({
      tempFilePath
    })
  },
  onChange(e) {
    this.setData({
      text: e.detail
    })
  },
  async submit() {
    if (!this.data.text) {
      Toast('请输入问题')
      return
    }
    wx.showLoading({
      title: '努力提交中...',
    })
    await db.collection('feedback').add({
      data: {
        text: this.data.text,
        image: this.data.tempFilePath
      }
    })
    wx.hideLoading()
    Toast('提交成功')
    wx.navigateBack()
  }
})