// miniprogram/pages/my/setting/setting.js
import {
  wxp
}
from '../../../util/wxp'
import area from '../../../util/area'
import Toast from '@vant/weapp/toast/toast';

const db = wx.cloud.database()
const _ = db.command

Page({
  mixins: [require('../../../mixins/pageMixins')],
  /**
   * 页面的初始数据
   */
  data: {
    from: null,
    areaList: area,
    show: false,
    popup: null,
    columns: [],
    communities: [],
    community: null,
    form: {
      community: null,
      buildingNo: null,
      unitNo: null,
      roomNo: null,
      name: null,
      phone: null,
      age: null,
      longLive: true,
      jiguan: null,
      jiguanStr: null
    },
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: async function (options) {
    this.setData({
      from: options.from || null
    })
    const {
      data
    } = await db.collection('user')
      .doc(getApp().globalData.openId)
      .get()
    const baseInfo = data.baseInfo || {}
    this.setData({
      form: Object.assign(this.data.form, baseInfo)
    })

    if (baseInfo.community) {
      const {
        data
      } = await db.collection('community')
        .doc(baseInfo.community)
        .get()
      this.setData({
        community: data
      })
    }
    const {
      data: communities
    } = await db.collection('community').where({
      status: 0
    }).get()
    this.setData({
      communities
    })
  },
  onCommChange: function (e) {
    const {
      value
    } = e.detail;
    this.setData({
      ['form.community']: value._id,
      community: value
    })
    this.closePopup()
  },
  onJiguanChange: function (e) {
    const {
      values
    } = e.detail;
    const jiguanStr = values.map(d => d.name).join(' ')
    this.setData({
      ['form.jiguan']: values[2].code,
      ['form.jiguanStr']: jiguanStr,
    })
    this.closePopup()
  },
  saveInfo: async function () {

    if (!this.data.form.community) {
      Toast('请选择您的小区');
      return
    }

    if (!this.data.form.buildingNo) {
      Toast('请输入您的楼栋号');
      return
    }

    if (!this.data.form.unitNo) {
      Toast('请输入您的单元号');
      return
    }

    if (!this.data.form.roomNo) {
      Toast('请输入您的门牌号');
      return
    }

    if (!this.data.form.name) {
      Toast('请输入您的姓名');
      return
    }

    if (!this.data.form.phone) {
      Toast('请输入您的联系电话');
      return
    }

    if (!this.data.form.jiguan) {
      Toast('请选择您的籍贯');
      return
    }
    if (!this.data.form.age) {
      Toast('请输入您的年龄');
      return
    }

    const {
      form: baseInfo
    } = this.data
    await db.collection('user').doc(getApp().globalData.openId)
      .update({
        data: {
          baseInfo,
          updateTime: db.serverDate()
        }
      })

    // 增加自己为默认家人
    const {
      data
    } = await db.collection('homemate').where({
      '_openid': getApp().globalData.openId,
      relation: '我自己'
    }).limit(1).get()
    if (data[0]) {
      await db.collection('homemate').doc(data[0]._id).update({
        data: {
          age: baseInfo.age,
          longLive: baseInfo.longLive,
          name: baseInfo.name
        }
      })
    } else {
      await db.collection('homemate').add({
        data: {
          age: baseInfo.age,
          longLive: baseInfo.longLive,
          name: baseInfo.name,
          relation: '我自己'
        }
      })
    }

    if (this.data.from == 'login') {
      wx.switchTab({
        url: '/pages/index/index',
      })
    } else {
      this.getPrevPage().getUserInfo()
      wx.navigateBack()
    }
  },
  async onPullDownRefresh() {
    wx.stopPullDownRefresh()
    this.setData({
      communities: []
    })
    wx.showLoading({
      title: '加载中',
    })
    const {
      data: communities
    } = await db.collection('community').where({
      status: 0
    }).get()
    this.setData({
      communities
    })
    wx.hideLoading()
  }
})